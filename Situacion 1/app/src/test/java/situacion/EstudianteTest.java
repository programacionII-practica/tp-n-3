package situacion;
import org.junit.Test;
import static org.junit.Assert.*;

public class EstudianteTest 
{
    public void ConstructorEstudiante()
    {
        Estudiante estudiante = new Estudiante("Gustavo Contreras", 19, 43995185, "Capital");
        assertEquals("Gustavo Contreras",estudiante.getNombre());
        assertEquals((int)19,estudiante.getEdad());
        assertEquals((int)43995185,estudiante.getDNI());
        assertEquals("Capital",estudiante.getLugarNacimiento());

        estudiante.agregarCalificacion(8);
        estudiante.agregarCalificacion(7);   // Promedio = (8+5+8)/3 --> 21/3 --> 7 
        estudiante.agregarCalificacion(8);

        assertEquals((int)7,estudiante.recibirPromedio());
    }
}
