package situacion;

import org.junit.Test;
import static org.junit.Assert.*;

public class CursoTest 
{
    @Test
    public void ConstructorCurso()
    {
        Curso curso = new Curso("1A", 20);
        assertEquals("1A",curso.getNombreCurso());
        assertEquals((int)20,curso.getCodigo());
    }
    @Test
    public void resetearNotas()
    {
        Estudiante estudiante1 = new Estudiante("Gustavo Contreras", 19, 43995185, "Capital");
        estudiante1.agregarCalificacion(8);
        estudiante1.agregarCalificacion(7);   
        estudiante1.agregarCalificacion(8);

        Estudiante estudiante2 = new Estudiante("Juan Domingo",18,40399183,"Fray Mamerto Esquiu");
        estudiante2.agregarCalificacion(4);
        estudiante2.agregarCalificacion(5);   
        estudiante2.agregarCalificacion(6);

        Estudiante estudiante3 = new Estudiante("Josefina Sabado", 18, 42949383, "Valle Viejo");
        estudiante3.agregarCalificacion(9);
        estudiante3.agregarCalificacion(10);
        estudiante3.agregarCalificacion(8);

        Curso curso = new Curso("1A", 20);
        curso.agregarEstudiante(estudiante1);
        curso.agregarEstudiante(estudiante2);
        curso.agregarEstudiante(estudiante3);

        curso.resetearNotas();

        assertEquals((int)0,estudiante1.recibirPromedio());
        assertEquals((int)0,estudiante2.recibirPromedio());
        assertEquals((int)0,estudiante3.recibirPromedio());
    }
    @Test
    public void cantidadDeEstudiantesInscriptos()
    {
        Estudiante estudiante1 = new Estudiante("Gustavo Contreras", 19, 43995185, "Capital");
        Estudiante estudiante2 = new Estudiante("Juan Domingo",18,40399183,"Fray Mamerto Esquiu");
        Estudiante estudiante3 = new Estudiante("Josefina Sabado", 18, 42949383, "Valle Viejo");

        Curso curso = new Curso("1A", 20);
        curso.agregarEstudiante(estudiante1);
        curso.agregarEstudiante(estudiante2);
        curso.agregarEstudiante(estudiante3);

        assertEquals((int)3,curso.cantidadDeEstudiantesInscriptos());
    }
    @Test
    public void estudiantes()
    {
        Estudiante estudiante1 = new Estudiante("Gustavo Contreras", 19, 43995185, "Capital");
        Estudiante estudiante2 = new Estudiante("Juan Domingo",18,40399183,"Fray Mamerto Esquiu");
        Estudiante estudiante3 = new Estudiante("Josefina Sabado", 18, 42949383, "Valle Viejo");

        Curso curso = new Curso("1A", 20);
        curso.agregarEstudiante(estudiante1);
        curso.agregarEstudiante(estudiante2);
        curso.agregarEstudiante(estudiante3);

        assertEquals("Gustavo Contreras",curso.estudiantes().get(0).getNombre());
        assertEquals("Juan Domingo",curso.estudiantes().get(1).getNombre());
        assertEquals("Josefina Sabado",curso.estudiantes().get(2).getNombre());
    }
    @Test
    public void estudiantesAprobados()
    {
        Estudiante estudiante1 = new Estudiante("Gustavo Contreras", 19, 43995185, "Capital");
        estudiante1.agregarCalificacion(2);
        estudiante1.agregarCalificacion(3);   
        estudiante1.agregarCalificacion(4);

        Estudiante estudiante2 = new Estudiante("Juan Domingo",18,40399183,"Fray Mamerto Esquiu");
        estudiante2.agregarCalificacion(7);
        estudiante2.agregarCalificacion(5);   
        estudiante2.agregarCalificacion(6);

        Estudiante estudiante3 = new Estudiante("Josefina Sabado", 18, 42949383, "Valle Viejo");
        estudiante3.agregarCalificacion(9);
        estudiante3.agregarCalificacion(10);
        estudiante3.agregarCalificacion(8);

        Curso curso = new Curso("1A", 20);
        curso.agregarEstudiante(estudiante1);
        curso.agregarEstudiante(estudiante2);
        curso.agregarEstudiante(estudiante3);

        assertEquals("Juan Domingo",curso.estudiantesAprobados().get(0).getNombre());
        assertEquals("Josefina Sabado",curso.estudiantesAprobados().get(1).getNombre());

        boolean seEncuentra=false;
        for (Estudiante i : curso.estudiantesAprobados())
        {
            if (i.getNombre() == "Gustavo Contreras")
            {
                seEncuentra = true;
            }
        }
        assertFalse(seEncuentra);
    }
    @Test
    public void existeUnEstudianteTrue()
    {
        Estudiante estudiante1 = new Estudiante("Gustavo Contreras", 19, 43995185, "Capital");
        Curso curso = new Curso("1A", 20);
        curso.agregarEstudiante(estudiante1);

        assertTrue(curso.existeEstudiante());
    }
    @Test
    public void existeMasDeUnEstudiante()
    {
        Estudiante estudiante1 = new Estudiante("Gustavo Contreras", 19, 43995185, "Capital");
        Estudiante estudiante2 = new Estudiante("Juan Domingo",18,40399183,"Fray Mamerto Esquiu");
        Estudiante estudiante3 = new Estudiante("Josefina Sabado", 18, 42949383, "Valle Viejo");

        Curso curso = new Curso("1A", 20);
        curso.agregarEstudiante(estudiante1);
        curso.agregarEstudiante(estudiante2);
        curso.agregarEstudiante(estudiante3);

        assertTrue(curso.existeEstudiante());
    }
    @Test
    public void existeUnEstudianteFalse()
    {
        Curso curso = new Curso("1A", 20);
        assertFalse(curso.existeEstudiante());
    }
    @Test
    public void existeEstudianteConNotaDiezTrue()
    {
        Estudiante estudiante1 = new Estudiante("Gustavo Contreras", 19, 43995185, "Capital");
        estudiante1.agregarCalificacion(10);
        estudiante1.agregarCalificacion(10);   
        estudiante1.agregarCalificacion(10);
        
        Curso curso = new Curso("1A", 20);
        curso.agregarEstudiante(estudiante1);

        assertTrue(curso.existeEstudianteConNotaDiez());
    }
    @Test
    public void existeEstudianteConNotaDiezFalse()
    {
        Estudiante estudiante1 = new Estudiante("Gustavo Contreras", 19, 43995185, "Capital");
        estudiante1.agregarCalificacion(5);
        estudiante1.agregarCalificacion(10);   
        estudiante1.agregarCalificacion(10);
        
        Curso curso = new Curso("1A", 20);
        curso.agregarEstudiante(estudiante1);

        assertFalse(curso.existeEstudianteConNotaDiez());
    }
    @Test
    public void existeEstudianteLlamadoTrue()
    {
        Estudiante estudiante2 = new Estudiante("Juan Domingo",18,40399183,"Fray Mamerto Esquiu");
        Curso curso = new Curso("1A", 20);
        curso.agregarEstudiante(estudiante2);

        assertTrue(curso.existeEstudianteLlamado("Juan Domingo"));
    }
    @Test
    public void existeEstudianteLlamadoFalse()
    {
        Estudiante estudiante1 = new Estudiante("Gustavo Contreras", 19, 43995185, "Capital");
        Curso curso = new Curso("1A", 20);
        curso.agregarEstudiante(estudiante1);

        assertFalse(curso.existeEstudianteLlamado("Juan Domingo"));
    }
    @Test
    public void porcentajeDeAprobados()
    {
        Estudiante estudiante1 = new Estudiante("Gustavo Contreras", 19, 43995185, "Capital");
        estudiante1.agregarCalificacion(2);
        estudiante1.agregarCalificacion(3);   
        estudiante1.agregarCalificacion(4);

        Estudiante estudiante2 = new Estudiante("Juan Domingo",18,40399183,"Fray Mamerto Esquiu");
        estudiante2.agregarCalificacion(7);
        estudiante2.agregarCalificacion(5);   
        estudiante2.agregarCalificacion(6);

        Estudiante estudiante3 = new Estudiante("Josefina Sabado", 18, 42949383, "Valle Viejo");
        estudiante3.agregarCalificacion(9);
        estudiante3.agregarCalificacion(10);
        estudiante3.agregarCalificacion(8);

        Curso curso = new Curso("1A", 20);
        curso.agregarEstudiante(estudiante1);
        curso.agregarEstudiante(estudiante2);
        curso.agregarEstudiante(estudiante3);

        assertEquals((int)66,curso.porcentajeDeAprobados());
    }
    @Test
    public void promedioDeCalificaciones()
    {
        Estudiante estudiante1 = new Estudiante("Gustavo Contreras", 19, 43995185, "Capital");
        estudiante1.agregarCalificacion(2);
        estudiante1.agregarCalificacion(3);   // Promedio = 3
        estudiante1.agregarCalificacion(4);

        Estudiante estudiante2 = new Estudiante("Juan Domingo",18,40399183,"Fray Mamerto Esquiu");
        estudiante2.agregarCalificacion(7);
        estudiante2.agregarCalificacion(5);   // Promedio = 6
        estudiante2.agregarCalificacion(6);

        Estudiante estudiante3 = new Estudiante("Josefina Sabado", 18, 42949383, "Valle Viejo");
        estudiante3.agregarCalificacion(9);
        estudiante3.agregarCalificacion(10); // Promedio = 9
        estudiante3.agregarCalificacion(8);

        Curso curso = new Curso("1A", 20);
        curso.agregarEstudiante(estudiante1);
        curso.agregarEstudiante(estudiante2);   // Promedio general = 6
        curso.agregarEstudiante(estudiante3);

        assertEquals((int)6,curso.promedioDeCalificaciones());
    }
    @Test
    public void estudiantesDelInteriorProvincial()
    {
        Estudiante estudiante1 = new Estudiante("Gustavo Contreras", 19, 43995185, "Capital");
        Estudiante estudiante2 = new Estudiante("Juan Domingo",18,40399183,"Fray Mamerto Esquiu");
        Estudiante estudiante3 = new Estudiante("Josefina Sabado", 18, 42949383, "Valle Viejo");

        Curso curso = new Curso("1A", 20);
        curso.agregarEstudiante(estudiante1);
        curso.agregarEstudiante(estudiante2);
        curso.agregarEstudiante(estudiante3);

        assertEquals("Juan Domingo",curso.estudiantesDelInteriorProvincial().get(0).getNombre());
        assertEquals("Josefina Sabado",curso.estudiantesDelInteriorProvincial().get(1).getNombre());
    }
    @Test
    public void ciudadesExceptoCapital()
    {
        Estudiante estudiante1 = new Estudiante("Gustavo Contreras", 19, 43995185, "Capital");
        Estudiante estudiante2 = new Estudiante("Juan Domingo",18,40399183,"Fray Mamerto Esquiu");
        Estudiante estudiante3 = new Estudiante("Josefina Sabado", 18, 42949383, "Valle Viejo");

        Curso curso = new Curso("1A", 20);
        curso.agregarEstudiante(estudiante1);
        curso.agregarEstudiante(estudiante2);
        curso.agregarEstudiante(estudiante3);

        assertEquals("Fray Mamerto Esquiu",curso.ciudadesExceptoCapital().get(0));
        assertEquals("Valle Viejo",curso.ciudadesExceptoCapital().get(1));
    }
    @Test
    public void unDesastreTrue()
    {
        Estudiante estudiante1 = new Estudiante("Gustavo Contreras", 19, 43995185, "Capital");
        estudiante1.agregarCalificacion(4);
        estudiante1.agregarCalificacion(3);   
        estudiante1.agregarCalificacion(4);

        Estudiante estudiante2 = new Estudiante("Juan Domingo",18,40399183,"Fray Mamerto Esquiu");
        estudiante2.agregarCalificacion(1);
        estudiante2.agregarCalificacion(2);   
        estudiante2.agregarCalificacion(3);

        Estudiante estudiante3 = new Estudiante("Josefina Sabado", 18, 42949383, "Valle Viejo");
        estudiante3.agregarCalificacion(4);
        estudiante3.agregarCalificacion(5); 
        estudiante3.agregarCalificacion(1);

        Curso curso = new Curso("1A", 20);
        curso.agregarEstudiante(estudiante1);
        curso.agregarEstudiante(estudiante2);
        curso.agregarEstudiante(estudiante3);

        assertTrue(curso.unDesastre());
    }
    @Test
    public void unDesastreFalse()
    {
        Estudiante estudiante1 = new Estudiante("Gustavo Contreras", 19, 43995185, "Capital");
        estudiante1.agregarCalificacion(7);
        estudiante1.agregarCalificacion(8);   
        estudiante1.agregarCalificacion(6);

        Estudiante estudiante2 = new Estudiante("Juan Domingo",18,40399183,"Fray Mamerto Esquiu");
        estudiante2.agregarCalificacion(1);
        estudiante2.agregarCalificacion(2);   
        estudiante2.agregarCalificacion(3);

        Estudiante estudiante3 = new Estudiante("Josefina Sabado", 18, 42949383, "Valle Viejo");
        estudiante3.agregarCalificacion(4);
        estudiante3.agregarCalificacion(5); 
        estudiante3.agregarCalificacion(1);

        Curso curso = new Curso("1A", 20);
        curso.agregarEstudiante(estudiante1);
        curso.agregarEstudiante(estudiante2);
        curso.agregarEstudiante(estudiante3);

        assertFalse(curso.unDesastre());
    }
}
