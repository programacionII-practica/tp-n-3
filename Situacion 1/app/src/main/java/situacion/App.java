/*
• #resetearNotas
    "Pone en cero las calificaciones de todos los estudiantes. "
• #agregarEstudiante: unEstudiante
    "Agrega unEstudiante al curso"
• #cantidadDeEstudiantesInscriptos
    "Retorna la cantidad de alumnos que se inscribieron al curso"
• #estudiantes
    "Retorna la colección de estudiantes del curso"
• #estudiantesAprobados
    "Retorna una colección con todos los estudiantes que aprobaron el curso (calificación
    superior o igual a 4)"
• #existeEstudiante: unEstudiante
    "Indica si unEstudiante se encuentra inscripto en el curso"
• #existeEstudianteConNotaDiez
    "Determina si algún alumno obtuvo la calificación 10"
• #existeEstudianteLlamado: aString
    "Indica si el estudiante llamado aString se encuentra inscripto en el curso"
• #porcentajeDeAprobados
    "Retorna en porcentaje de estudiantes aprobados"
• #promedioDeCalificaciones
    "Calcula el promedio de las calificaciones obtenidas por los alumnos"
• #estudiantesDelInteriorProvinial
    "Retorna una colección con todos los estudiantes que no nacieron en la capital"
• #ciudadesExceptoCapital
    "Retorna una colección, sin repeticiones, conteniendo los nombres de todas las
    ciudades donde nacieron los alumnos inscriptos al curso"
• #unDesastre 
    "Retorna verdadero si todos los estudiantes desaprobaron el curso" 
*/
package situacion;

public class App {
    public static void main(String args[])
    {
        Estudiante estudiante1 = new Estudiante("Gustavo Contreras", 19, 43995185, "Capital");
        estudiante1.agregarCalificacion(2);
        estudiante1.agregarCalificacion(3);   
        estudiante1.agregarCalificacion(4);

        Estudiante estudiante2 = new Estudiante("Juan Domingo",18,40399183,"Fray Mamerto Esquiu");
        estudiante2.agregarCalificacion(7);
        estudiante2.agregarCalificacion(5);   
        estudiante2.agregarCalificacion(6);

        Estudiante estudiante3 = new Estudiante("Josefina Sabado", 18, 42949383, "Valle Viejo");
        estudiante3.agregarCalificacion(9);
        estudiante3.agregarCalificacion(10);
        estudiante3.agregarCalificacion(8);

        Curso curso = new Curso("1A", 20);
        curso.agregarEstudiante(estudiante1);
        System.out.println("Ingresa el alumno " + curso.estudiantes().get(0).getNombre() + " al curso " + curso.getNombreCurso());
        curso.agregarEstudiante(estudiante2);
        System.out.println("Ingresa el alumno " + curso.estudiantes().get(1).getNombre() + " al curso " + curso.getNombreCurso());
        curso.agregarEstudiante(estudiante3);
        System.out.println("Ingresa el alumno " + curso.estudiantes().get(2).getNombre() + " al curso " + curso.getNombreCurso());

        System.out.println("\n\nLa cantidad de alumnos inscriptos en " + curso.getNombreCurso() +": " + curso.cantidadDeEstudiantesInscriptos());
        System.out.println("Los estudiantes inscriptos: ");
        for (Estudiante i : curso.estudiantes())
        {
            System.out.println("    - " + i.getNombre());
        }
        System.out.println("Los estudiantes aprobados: ");
        for (Estudiante i : curso.estudiantesAprobados())
        {
            System.out.println("    - " + i.getNombre());
        }
        System.out.println("Porcentaje de aprobados: " + curso.porcentajeDeAprobados() + "%");
        System.out.println("Promedio de calificaciones: " + curso.promedioDeCalificaciones());
        System.out.println("Los estudiantes del interior de la provincia: ");
        for (Estudiante i : curso.estudiantesDelInteriorProvincial())
        {
            System.out.println("    - " + i.getNombre());
        }
        System.out.println("Ciudades donde nacieron los alumnos (excepto Capital): ");
        for (String i : curso.ciudadesExceptoCapital())
        {
            System.out.println("    - " + i);
        }
        if (curso.unDesastre())
        {
            System.out.print("El curso es un desastre, todos estan desaprobados!");
        }
    }
}
