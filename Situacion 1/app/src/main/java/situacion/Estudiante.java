package situacion;
import java.util.ArrayList;
public class Estudiante {
    private String nombre;
    private int edad;
    private int dni;
    private String lugarNacimiento;
    private ArrayList<Integer> calificacion;
    

    public Estudiante(String nombre,int edad,int dni,String lugarNacimiento)
    {
        this.nombre = nombre;
        this.edad = edad;
        this.dni = dni;
        this.lugarNacimiento = lugarNacimiento;
        calificacion = new ArrayList<Integer>();
    }

    public void agregarCalificacion(int calificacion)
    {
        this.calificacion.add(calificacion);
    }
    public int recibirPromedio()
    {
        int promedio = 0;
        for (int i : this.calificacion)
        {
            promedio += i;
        }
        promedio = promedio/this.calificacion.size();
        
        return promedio;
    }
    public String getLugarNacimiento()
    {
        return this.lugarNacimiento;
    }
    public String getNombre()
    {
        return this.nombre;
    }
    public int getEdad()
    {
        return this.edad;
    }
    public int getDNI()
    {
        return this.dni;
    }
    public ArrayList<Integer> getCalificacion()
    {
        return this.calificacion;
    }
}
