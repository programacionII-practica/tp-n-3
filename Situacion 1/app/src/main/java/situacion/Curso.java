package situacion;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
public class Curso 
{
    private String nombreCurso;
    private int codigo;
    private ArrayList<Estudiante> estudiantes;
    
    public Curso(String nombreCurso,int codigo)
    {
        this.nombreCurso = nombreCurso;
        this.codigo = codigo;
        estudiantes = new ArrayList<Estudiante>();
    }
    public String getNombreCurso()
    {
        return this.nombreCurso;
    }
    public int getCodigo()
    {
        return this.codigo;
    }

    public boolean resetearNotas()
    {
        for (Estudiante estudiante : estudiantes)
        {
            estudiante.getCalificacion().clear();
            estudiante.getCalificacion().add(0);
        }

        return true;
    }

    public void agregarEstudiante(Estudiante estudiante)
    {
        this.estudiantes.add(estudiante);
    }
    public int cantidadDeEstudiantesInscriptos()
    {
        return this.estudiantes.size();
    }
    public ArrayList<Estudiante> estudiantes()
    {
        return this.estudiantes;
    }
    public ArrayList<Estudiante> estudiantesAprobados()
    {
        ArrayList<Estudiante> estudiantesAprobados = new ArrayList<Estudiante>();
        for (Estudiante i : this.estudiantes)
        {
            if (i.recibirPromedio() >= 4)
            {
                estudiantesAprobados.add(i);
            }   
        }
        return estudiantesAprobados;
    }
    public boolean existeEstudiante()
    {
        return !estudiantes.isEmpty();
    }
    public boolean existeEstudianteConNotaDiez()
    {
        boolean existe=false;
        for (Estudiante estudiante : this.estudiantes)
        {
            if (estudiante.recibirPromedio() == 10)
            {
                existe = true;
            }
        }
        return existe;
    }
    public boolean existeEstudianteLlamado(String nombre)
    {
        boolean existe=false;
        for (Estudiante estudiante : this.estudiantes)
        {
            if (estudiante.getNombre() == nombre)
            {
                existe = true;
            }
        }
        return existe;
    }
    public int porcentajeDeAprobados()
    {
        int cantidadAprobados=0;
        for (Estudiante estudiante : this.estudiantes)
        {
            if (estudiante.recibirPromedio() >= 4)
            {
                cantidadAprobados += 1;
            }
        }
        return (cantidadAprobados*100)/estudiantes.size();
    }
    public int promedioDeCalificaciones()
    {
        int cantidadPromedios=0;
        for (Estudiante estudiante : this.estudiantes)
        {
            cantidadPromedios += estudiante.recibirPromedio();
        }
        return cantidadPromedios/estudiantes.size();
    }
    public ArrayList<Estudiante> estudiantesDelInteriorProvincial()
    {
        ArrayList<Estudiante> estudiantesDelInterior = new ArrayList<Estudiante>();
        for (Estudiante estudiante : this.estudiantes)
        {
            if (estudiante.getLugarNacimiento() != "Capital")
            {
                estudiantesDelInterior.add(estudiante);
            }
        }
        return estudiantesDelInterior;
    }
    public ArrayList<String> ciudadesExceptoCapital()
    {
        ArrayList<String> ciudades = new ArrayList<String>();
        for (Estudiante estudiante : this.estudiantes)
        {
            if (estudiante.getLugarNacimiento() != "Capital")
            {
                ciudades.add(estudiante.getLugarNacimiento());
            }
        }

        Set<String> ciudadesNoRepetidas = new HashSet<>(ciudades);
        ciudades.clear();
        ciudades.addAll(ciudadesNoRepetidas);

        return ciudades;
    }
    public boolean unDesastre()
    {
        boolean desastre=true;
        for (Estudiante estudiante : this.estudiantes)
        {
            if (estudiante.recibirPromedio() >= 4)
            {
                desastre = false;
            }
        }
        return desastre;
    }


}
