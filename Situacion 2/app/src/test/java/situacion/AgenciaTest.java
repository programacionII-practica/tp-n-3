package situacion;

import org.junit.Test;
import static org.junit.Assert.*;

public class AgenciaTest 
{
    @Test public void nombre()
    {
        Agencia agencia = new Agencia("Estebanquito");
        assertEquals("Estebanquito", agencia.getNombreAgencia());
    }
    @Test public void registrarVehiculo()
    {
        Agencia agencia = new Agencia("Estebanquito");
        Vehiculo vehiculo1 = new Vehiculo("Ford", "OWO-404", 2000, "Auto", 200, 4);
        Vehiculo vehiculo2 = new Vehiculo("Volkswagen","AWA-707",3000,"Camioneta",100,6);

        agencia.registrarVehiculo(vehiculo1);
        agencia.registrarVehiculo(vehiculo2);

        assertEquals("OWO-404",agencia.listaVehiculos().get(0).getPatente());
        assertEquals("AWA-707",agencia.listaVehiculos().get(1).getPatente());
    }
    @Test public void seEncuentraVehiculoAuto()
    {
        Vehiculo vehiculo1 = new Vehiculo("Ford", "OWO-404", 2000, "Auto", 200, 4);
        Agencia agencia = new Agencia("Test");

        agencia.registrarVehiculo(vehiculo1);

        assertEquals(vehiculo1, agencia.seEncuentraVehiculo("Auto"));

        Vehiculo auto2 = new Vehiculo("Volkswagen","KWY-906",3000,"Auto",300,4);
        agencia.registrarVehiculo(auto2);

        assertEquals(auto2, agencia.seEncuentraVehiculo("Auto"));
    }
    @Test public void seEncuentraVehiculoMinibus()
    {
        Vehiculo minibus = new Vehiculo("Toyota", "SKD-249", 5000, "Minibus", 3000, 8);
        Agencia agencia = new Agencia("Test");

        agencia.registrarVehiculo(minibus);

        assertEquals(minibus, agencia.seEncuentraVehiculo("Minibus"));
    }
    @Test public void seEncuentraVehiculoCamioneta()
    {
        Vehiculo camioneta = new Vehiculo("Ford", "OKY-202", 4000, "Camioneta", 500, 2);
        Agencia agencia = new Agencia("Test");

        agencia.registrarVehiculo(camioneta);

        assertEquals(camioneta, agencia.seEncuentraVehiculo("Camioneta"));
    }
    @Test public void seEncuentraVehiculoCamion()
    {
        Vehiculo camion = new Vehiculo("Volvo", "KTD-239", 10000, "Camion", 1000, 2);
        Agencia agencia = new Agencia("Test");

        agencia.registrarVehiculo(camion);

        assertEquals(camion, agencia.seEncuentraVehiculo("Camion"));
    }
    @Test public void calcularPrecioAuto()
    {
        Agencia agencia = new Agencia("Estebanquito");
        Vehiculo vehiculo = new Vehiculo("Ford", "OWO-404", 2000, "Auto", 200, 4);

        assertEquals((int)10200,agencia.calcularPrecio(vehiculo, 5));
    }
    @Test public void calcularPrecioMinibus()
    {
        Agencia agencia = new Agencia("Estebanquito");
        Vehiculo vehiculo = new Vehiculo("Ford", "OWO-404", 2000, "Minibus", 200, 4);

        assertEquals((int)11000,agencia.calcularPrecio(vehiculo, 5));
    }
    @Test public void calcularPrecioCamioneta50KM()
    {
        Agencia agencia = new Agencia("Estebanquito");
        Vehiculo vehiculo = new Vehiculo("Ford", "OWO-404", 2000, "Camioneta", 40, 4);

        assertEquals((int)10300,agencia.calcularPrecio(vehiculo, 5));
    }
    @Test public void calcularPrecioCamionetaMasDe50KM()
    {
        Agencia agencia = new Agencia("Estebanquito");
        Vehiculo vehiculo = new Vehiculo("Ford", "OWO-404", 2000, "Camioneta", 200, 4);

        assertEquals((int)14000,agencia.calcularPrecio(vehiculo, 5));
    }
    @Test public void calcularPrecioCamion()
    {
        Agencia agencia = new Agencia("Estebanquito");
        Vehiculo vehiculo = new Vehiculo("Ford", "OWO-404", 2000, "Camion", 200, 4);

        assertEquals((int)10200,agencia.calcularPrecio(vehiculo, 5));
    }
}
