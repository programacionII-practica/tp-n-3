package situacion;

import org.junit.Test;
import static org.junit.Assert.*;

public class VehiculoTest 
{
    @Test
    public void Vehiculo()
    {
        Vehiculo vehiculo = new Vehiculo("Ford", "OWO-404", 2000, "Auto", 200, 4);
        assertEquals("Ford", vehiculo.getMarca());
        assertEquals("OWO-404", vehiculo.getPatente());
        assertEquals((int)2000, vehiculo.getPrecioBasePorDia());
        assertEquals("Auto", vehiculo.getTipoVehiculo());
        assertEquals((int)200, vehiculo.getKilometraje());
        assertEquals((int)4, vehiculo.getNumeroPlazas());
    }
}
