package situacion;

import java.util.ArrayList;
public class Agencia 
{
    String nombreAgencia;
    ArrayList<Vehiculo> vehiculos;

    public Agencia(String nombreAgencia) {
        this.nombreAgencia = nombreAgencia;
        vehiculos = new ArrayList<Vehiculo>();
    }
    public String getNombreAgencia()
    {
        return this.nombreAgencia;
    }
    public void registrarVehiculo(Vehiculo vehiculo)
    {
        vehiculos.add(vehiculo);
    }
    public ArrayList<Vehiculo> listaVehiculos()
    {
        return this.vehiculos;
    }

    public Vehiculo seEncuentraVehiculo(String tipoVehiculo)
    {
        Vehiculo vehiculoAlquilado=null;
        for (Vehiculo i : vehiculos)
        {
            if (i.getTipoVehiculo() == tipoVehiculo)
            {
                vehiculoAlquilado = i;
            }
        }
        return vehiculoAlquilado;
    }

    public int calcularPrecio(Vehiculo vehiculo, int diasAlquilado)
    {
        int precio;
        precio = vehiculo.getPrecioBasePorDia() * diasAlquilado;

        if (vehiculo.getTipoVehiculo() == "Auto")
        {
            precio+= 50*vehiculo.getNumeroPlazas();
        }
        else if (vehiculo.getTipoVehiculo() == "Minibus")
        {
            precio+= 250*vehiculo.getNumeroPlazas();
        }
        else if (vehiculo.getTipoVehiculo() == "Camioneta")
        {
            if (vehiculo.getKilometraje() <= 50)
            {
                precio += 300;
            }
            else
            {
                precio += 20*vehiculo.getKilometraje();
            }
        }
        else
        {
            precio += 200;
        }

        return precio;
    }
 
}
