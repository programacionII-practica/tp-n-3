/*
Se debe programar una aplicación para una agencia de alquiler de autos. Se necesita poder determinar
el precio de alquiler de los vehículos con que cuenta la empresa que alquila vehículos tanto de pasajeros como de carga. De los vehículos se almacena marca, patente, el precio base de alquiler. Los
vehículos de alquiler pueden ser autos, minibús, camionetas y camiones. Los vehículos se deben registrar al ingresar al parque automotor para estar disponibles para alquilar o vender. El precio de
alquiler se compone de un precio base por día. En el caso de coches se suma de $50 por y plaza por
día. El precio de alquiler del microbús es igual al de los coches más $250 de seguro por plaza. El
precio de alquiler de los vehículos de carga es de $300 si el viaje es menor a 50 Km. En caso contrario
su precio será de $20 multiplicado por kilómetro recorrido. Para los camiones se debe abonar $200
extras independientemente del kilometraje recorrido. Se debe poder mostrar el precio obtenido para
cada caso.
 */
package situacion;

public class App {
    public static void main(String[] args) 
    {
        Agencia agencia = new Agencia("AutoShop");
        Vehiculo auto1 = new Vehiculo("Ford", "OWO-404", 2000, "Auto", 200, 4);
        Vehiculo auto2 = new Vehiculo("Volkswagen","KWY-906",3000,"Auto",300,4);
        Vehiculo camioneta = new Vehiculo("Ford", "OKY-202", 4000, "Camioneta", 500, 2); //Cambiar kilometraje
        Vehiculo camion = new Vehiculo("Volvo", "KTD-239", 10000, "Camion", 1000, 2);
        Vehiculo minibus = new Vehiculo("Toyota", "SKD-249", 5000, "Minibus", 3000, 8);

        System.out.println("Se registro un " + auto1.getTipoVehiculo() + " patente " + auto1.getPatente());
        agencia.registrarVehiculo(auto1);
        System.out.println("Se registro un " + auto2.getTipoVehiculo() + " patente " + auto2.getPatente());
        agencia.registrarVehiculo(auto2);
        System.out.println("Se registro una " + camioneta.getTipoVehiculo() + " patente " + camioneta.getPatente());
        agencia.registrarVehiculo(camioneta);
        System.out.println("Se registro un " + camion.getTipoVehiculo() + " patente " + camion.getPatente());
        agencia.registrarVehiculo(camion);
        System.out.println("Se registro un " + minibus.getTipoVehiculo() + " patente " + minibus.getPatente());
        agencia.registrarVehiculo(minibus);

        // Alquilo un vehiculo
        if (agencia.seEncuentraVehiculo("Auto") != null)    // Puede cambiarse el vehiculo alquilado, cambiando el tipo de vehiculo en esta linea
        {
            Vehiculo vehiculoAlquilado = agencia.seEncuentraVehiculo("Auto");
            System.out.println("Agencia: " + agencia.getNombreAgencia());
            System.out.println("\n\nMarca de vehiculo alquilado: " + vehiculoAlquilado.getMarca());
            System.out.println("Patente: " + vehiculoAlquilado.getPatente());
            System.out.println("Tipo de vehiculo " + vehiculoAlquilado.getTipoVehiculo());
            System.out.println("Kilometraje: " + vehiculoAlquilado.getKilometraje());
            System.out.println("Numero de plazas " + vehiculoAlquilado.getNumeroPlazas());
            System.out.println("Precio: " + agencia.calcularPrecio(vehiculoAlquilado, 5)); // El segundo parametro de calcularPrecio, son los dias alquilados. Esto altera el precio final
        }
    }
}
