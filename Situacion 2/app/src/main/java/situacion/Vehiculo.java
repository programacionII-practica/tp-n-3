package situacion;

public class Vehiculo 
{
    String marca;
    String patente;
    int precioBasePorDia;
    String tipoVehiculo;
    int kilometraje;
    int numeroPlazas;

    public Vehiculo(String marca, String patente, int precioBasePorDia, String tipoVehiculo, int kilometraje,int numeroPlazas) {
        this.marca = marca;
        this.patente = patente;
        this.precioBasePorDia = precioBasePorDia;
        this.tipoVehiculo = tipoVehiculo;
        this.kilometraje = kilometraje;
        this.numeroPlazas = numeroPlazas;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getPatente() {
        return patente;
    }

    public void setPatente(String patente) {
        this.patente = patente;
    }

    public int getPrecioBasePorDia() {
        return precioBasePorDia;
    }

    public void setPrecioBasePorDia(int precioBasePorDia) {
        this.precioBasePorDia = precioBasePorDia;
    }

    public String getTipoVehiculo() {
        return tipoVehiculo;
    }

    public void setTipoVehiculo(String tipoVehiculo) {
        this.tipoVehiculo = tipoVehiculo;
    }

    public int getKilometraje() {
        return kilometraje;
    }

    public void setKilometraje(int kilometraje) {
        this.kilometraje = kilometraje;
    }
    public int getNumeroPlazas()
    {
        return this.numeroPlazas;
    }
}
